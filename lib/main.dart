import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MovieFlix',
      theme: ThemeData(
        brightness: Brightness.dark,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'MovieFlix'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Card> cards;

  void initState() {
    super.initState();
    cards = _buildCardTile(11); //Contém os cards de filmes.
  }

  /*
  /Esta funcao gera uma lista de Cards baseado 
  /nos arquivos movie0.jpg até movie10.jpg.
  */
  List<Card> _buildCardTile(int count) => List.generate(
        count,
        (i) => Card(
              child: Image.asset('movies/movie$i.jpg'),
            ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(),  
      bottomNavigationBar: BottomNavigationBar(
       currentIndex: 0,
       items: [
         BottomNavigationBarItem(
           icon: new Icon(Icons.movie),
           title: new Text('Watch'),
         ),
         BottomNavigationBarItem(
           icon: Icon(Icons.search),
           title: Text('Search')
         ),
         BottomNavigationBarItem(
           icon: Icon(Icons.person),
           title: Text('Profile')
         )
       ],
     ),
    );
  }
}
